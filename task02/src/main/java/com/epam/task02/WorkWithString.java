package com.epam.task02;

import java.util.Scanner;

public class WorkWithString {

    Scanner scan;

    public WorkWithString(Scanner scan) {
        this.scan = scan;
        int itemMenu;
        int numberItemArray = 0;
        String inputLine[];

        while (true) {
            MyMenu.runStringMenu();
            itemMenu = MyAction.getNumberInt(scan, 1, 7);
            if (itemMenu == 7) {
                break;
            }
            System.out.println(" \nSpecify amount of lines: ");
            numberItemArray = MyAction.getNumberInt(scan, 3, 10);
            inputLine = new String[numberItemArray];
            MyAction.getStringLine(scan);
            for (int i = 0; i < inputLine.length; i++) {
                System.out.println("Input line " + i + ": ");
                inputLine[i] = MyAction.getStringLine(scan);
            }
//            for (int i = 0; i < inputLine.length; i++) {
//                System.out.println("Line " + i + ": " +  inputLine[i]);
//            }
            switch (itemMenu) {
                case 1:
                    ShortestLineAndLongestLine(inputLine);
                    break;
                case 2:
                    LinesLongerThanAverageLength(inputLine);
                    break;
                case 3:
                    LinesShorterThanAverageLength(inputLine);
                    break;
                case 4:
                    WordWithLowestAmountOfDifferentCharacters(inputLine);
                    break;
                case 5:
                    WordContainsOnlyDifferentCharacters(inputLine);
                    break;
                case 6:
                    WordContainsOnlyDigits(inputLine);
                    break;
            }
        }
    }

    private void ShortestLineAndLongestLine(String[] inputLine) {
        int minLength = 0;
        int minItem = 0;
        int maxLength = 0;
        int maxItem = 0;

        minLength = inputLine[0].length();
        maxLength = inputLine[0].length();
        for (int i = 1; i < inputLine.length; i++) {
            if (minLength > inputLine[i].length()) {
                minLength = inputLine[i].length();
                minItem = i;
            }
            if (maxLength < inputLine[i].length()) {
                maxLength = inputLine[i].length();
                maxItem = i;
            }
        }
        System.out.println("\n\nShortest line is " + inputLine[minItem]
                + " (length is " + minLength + " )");
        System.out.println("Longest line is " + inputLine[maxItem]
                + " (length is " + maxLength + " )");
    }

    private void LinesLongerThanAverageLength(String[] inputLine) {
        int avageLengt = 0;
        for (int i = 0; i < inputLine.length; i++) {
            avageLengt += inputLine[i].length();
        }
        avageLengt /= inputLine.length;
        System.out.println("\n\nLines longer than average length:");
        for (int i = 0; i < inputLine.length; i++) {
            if (inputLine[i].length() > avageLengt) {
                System.out.println(inputLine[i] + " (length is "
                        + inputLine[i].length() + " )");
            }
        }
    }

    private void LinesShorterThanAverageLength(String[] inputLine) {
        int avageLengt = 0;
        for (int i = 0; i < inputLine.length; i++) {
            avageLengt += inputLine[i].length();
        }
        avageLengt /= inputLine.length;
        System.out.println("\n\nLines shorter than average length:");
        for (int i = 0; i < inputLine.length; i++) {
            if (inputLine[i].length() < avageLengt) {
                System.out.println(inputLine[i] + " (length is "
                        + inputLine[i].length() + " )");
            }
        }
    }

    private void WordWithLowestAmountOfDifferentCharacters(String[] inputLine) {
        int minItemCharacters = 100;
        int minItem = -1;
        for (int i = 0; i < inputLine.length; i++) {
            String tempCharacters = inputLine[i].trim().substring(0, 1);
            int tempItem = 0;
            for (int j = 1; j < inputLine[i].length(); j++) {
                tempItem = tempCharacters.indexOf(inputLine[i].trim()
                        .substring(j, j + 1), 0);
                if (tempItem < 0) {
                    tempCharacters = tempCharacters.concat(inputLine[i]
                            .substring(j, j + 1));
                }
            }
            if (minItemCharacters > tempCharacters.length()) {
                minItemCharacters = tempCharacters.length();
                minItem = i;
            }
        }
        System.out.println("\nWord with lowest amount of different characters: ");
        System.out.println(inputLine[minItem]);
    }

    private void WordContainsOnlyDifferentCharacters(String[] inputLine) {
        int position = -1;
        for (int i = 0; i < inputLine.length; i++) {
            String tempCharacters = inputLine[i].trim().substring(0, 1);
            int tempItem = 0;
            for (int j = 1; j < inputLine[i].length(); j++) {
                tempItem = tempCharacters.indexOf(inputLine[i].trim().substring(j, j + 1), 0);
                if (tempItem < 0) {
                    tempCharacters = tempCharacters.concat(inputLine[i].substring(j, j + 1));
                } else {
                    tempItem = j;
                    break;
                }
            }
            if (tempItem < 0) {
                position = i;
                break;
            }
        }
        System.out.println("\nWord with lowest amount of different characters: ");
        if (position >= 0) {
            System.out.println(inputLine[position]);
        }
    }

    private void WordContainsOnlyDigits(String[] inputLine) {
        int position = -1;
        int tempItem = 0;
        for (int i = 0; i < inputLine.length; i++) {
            int temp = 0;
            for (int j = 0; j < inputLine[i].length(); j++) {
                temp = "0123456789".indexOf(inputLine[i].substring(j, j + 1), 0);
                if (temp < 0) {
                    break;
                }
            }
            if (temp >= 0) {
                position = i;
                ++tempItem;
            }
            if (tempItem == 2) {
                break;
            }
        }
        System.out.println("\nWord contains only digits:");
        if (position >= 0) {
            System.out.println(inputLine[position]);
        }
    }
}
