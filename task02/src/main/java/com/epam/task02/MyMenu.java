package com.epam.task02;

public class MyMenu {

  public static void runBasicMenu() {
      for (int i = 0; i < 5; i++) {
          System.out.println();
      }
      System.out.println("-----------------------------------------");
      System.out.println();
      System.out.println("Basic menu:");
      System.out.println("1. working with an array");
      System.out.println("2. working with a string");
      System.out.println("3. working with a calculator");
      System.out.println("4. Exit");
  }

  public static void runArrayMenu() {
      for (int i = 0; i < 5; i++) {
          System.out.println();
      }
      System.out.println("-----------------------------------------");
      System.out.println();
      System.out.println("Menu with an array:");
      System.out.println("1. Exchange max negative and min positive elements");
      System.out.println("2. Sum of elements on even positions");
      System.out.println("3. Replace negative values with 0");
      System.out.println("4. Multiply by 3 each positive element standing before negative one");
      System.out.println("5. Difference between average and min element in array");
      System.out.println("6. Elements which present more than one time and stay on odd index");
      System.out.println("7. Exit");
  }
  
  public static void runStringMenu(){
      for (int i = 0; i < 5; i++) {
          System.out.println();
      }
      System.out.println("-----------------------------------------");
      System.out.println();
      System.out.println("Menu with a string:");
      System.out.println("1. Shortest line and Longest line");
      System.out.println("2. Lines longer than average length");
      System.out.println("3. Lines shorter than average length");
      System.out.println("4. Word with lowest amount of different characters");
      System.out.println("5. Word contains only different characters");
      System.out.println("6. Word contains only digits");
      System.out.println("7. Exit");
  }
  
  public static void runCalculatorMenu(){
      for (int i = 0; i < 2; i++) {
          System.out.println();
      }
      System.out.println("-----------------------------------------");
      System.out.println();
      System.out.println("Menu with a calculator:");
      System.out.println("1. addition       (+)");
      System.out.println("2. subtraction    (-)");
      System.out.println("3. multiplication (*)");
      System.out.println("4. division       (/)");
      System.out.println("5. Exit");
  }
}
