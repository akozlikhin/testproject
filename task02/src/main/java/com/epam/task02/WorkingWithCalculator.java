package com.epam.task02;

import java.util.Scanner;

public class WorkingWithCalculator {

    Scanner scan;

    public WorkingWithCalculator(Scanner scan) {
        this.scan = scan;
        int itemMenu = 0;
        double number1;
        double number2;

        while (true) {
            MyMenu.runCalculatorMenu();
            itemMenu = MyAction.getNumberInt(scan, 1, 5);
            if (itemMenu == 5) {
                break;
            }
            System.out.println("");
            System.out.print("Input 1 number : ");
            number1 = MyAction.getNumberDouble(scan, -1000000, 1000000);
            while (true) {
                System.out.println("");
                System.out.print("Input 2 number : ");
                number2 = MyAction.getNumberDouble(scan, -1000000, 1000000);
                if (itemMenu == 4 & number2 == 0) {
                    System.out.println("you can't divide by 0");
                    continue;
                } else {
                    System.out.println("");
                    break;
                }
            }
            switch (itemMenu) {
                case 1:
                    System.out.println(number1 + " + " + number2 + " = "
                            + MyAction.add(number1, number2));
                    break;
                case 2:
                    System.out.println(number1 + " - " + number2 + " = "
                            + MyAction.subtruct(number1, number2));
                    break;
                case 3:
                    System.out.println(number1 + " * " + number2 + " = "
                            + MyAction.multiply(number1, number2));
                    break;
                case 4:
                    System.out.println(number1 + " / " + number2 + " = "
                            + MyAction.divide(number1, number2));
                    break;
            }
        }
    }
}
