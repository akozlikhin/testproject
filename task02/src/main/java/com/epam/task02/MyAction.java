package com.epam.task02;

import java.util.Scanner;

public class MyAction {

    public static int getRandomNumber(int min, int max) {
        return (int) ((Math.random() * (max - min)) + min);
    }

    public static int getNumberInt(Scanner scan, int minNumber, int maxNumber) {
        int number;
        while (true) {
            String inputItemMenu = scan.next();
            try {
                number = Integer.parseInt(inputItemMenu);
            } catch (NumberFormatException e) {
                System.out.println("Invalid input!");
                System.out.println("there must be a number");
                continue;
            }
            if (number < minNumber || number > maxNumber) {
                System.out.println("Invalid input!");
                System.out.println("there must be a number between " + minNumber
                        + " and " + maxNumber);
                continue;
            }
            break;
        }
        return number;
    }

    public static String getStringLine(Scanner scan) {
        String inputItemMenu = scan.nextLine();
        return inputItemMenu;
    }

    public static double getNumberDouble(Scanner scan, int minNumber, int maxNumber) {
        double number;
        while (true) {
            String inputItemMenu = scan.next();
            try {
                number = Double.parseDouble(inputItemMenu);
            } catch (NumberFormatException e) {
                System.out.println("Invalid input!");
                System.out.println("there must be a number");
                continue;
            }
            if (number < minNumber || number > maxNumber) {
                System.out.println("Invalid input!");
                System.out.println("there must be a number between " + minNumber
                        + " and " + maxNumber);
                continue;
            }
            break;
        }
        return number;
    }

    public static double add(double a, double b) {
        return a + b;
    }

    public static double subtruct(double a, double b) {
        return a - b;
    }

    public static double multiply(double a, double b) {
        return a * b;
    }

    public static double divide(double a, double b) {
        return a / b;
    }
}
