/**
 * 
 */
package com.epam.task02;

import java.util.Scanner;

/**
 *
 * @author akozlikhin
 */
public class WorkWithTasks {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String inputItemMenu = "";
        int itemMenu = 0;

        Scanner scan = new Scanner(System.in);
        while (4 != itemMenu) {
        MyMenu.runBasicMenu();
            itemMenu = MyAction.getNumberInt(scan, 1, 4);
            switch (itemMenu) {
                case 1:
                    new WorkWithArray(scan);
                    break;
                case 2:
                    new WorkWithString(scan);
                    break;
                case 3:
                new WorkingWithCalculator(scan);
            }
        }
        scan.close();
        System.out.println("Goodbye!");

    }
}
