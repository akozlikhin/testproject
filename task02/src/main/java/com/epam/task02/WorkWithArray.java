package com.epam.task02;

import java.util.Scanner;

public class WorkWithArray {

    private int min;
    private int max;
    private int numberItemArray = 0;
    Scanner scan;

    public WorkWithArray(Scanner scan) {
        this.scan = scan;
        int itemMenu = 0;

        System.out.println();
        System.out.println();
        System.out.println("array creation");
        System.out.println("enter the number of array elements: ");
        numberItemArray = MyAction.getNumberInt(scan, 2, 50);

        System.out.println("");
        System.out.println("enter the minimum element of the array: ");
        min = MyAction.getNumberInt(scan, -50, 40);

        System.out.println("");
        System.out.println("enter the maximum element of the array: ");
        max = MyAction.getNumberInt(scan, min + 1, 50);

        int[] demension = new int[numberItemArray];
        for (int i = 0; i < demension.length; i++) {
            demension[i] = MyAction.getRandomNumber(min, max);
        }

        MyMenu.runArrayMenu();
        while (7 != itemMenu) {
            itemMenu = MyAction.getNumberInt(scan, 1, 7);

            if (itemMenu != 7) {
                System.out.println("");
                System.out.println("Original array:");
                for (int i = 0; i < demension.length; i++) {
                    if (i % 14 == 0) {
                        System.out.println("");
                    }
                    System.out.print(demension[i] + "    ");
                }
                System.out.println("");
            }
            switch (itemMenu) {
                case 1:
                    exchangeMaxNegativeAndMinPositiveElements(demension.clone());
                    break;
                case 2:
                    sumOfElementsOnEvenPositions(demension.clone());
                    break;
                case 3:
                    replaceNegativeValuesWith0(demension.clone());
                    break;
                case 4:
                    multiplyBy3EachPositiveElementStandingBeforeNegativeOne(demension.clone());
                    break;
                case 5:
                    differenceBetweenAverageAndMinElementInArray(demension.clone());
                    break;
                case 6:
                    elementsWhichPresentMoreThanOneTimeAndStayOnOddIndex(demension.clone());
                    break;
            }
            MyMenu.runArrayMenu();
        }
    }

    private void exchangeMaxNegativeAndMinPositiveElements(int[] demention) {
        int maxNegative = -50;
        int numberMaxNegative = 0;
        int minPositive = 50;
        int numberMinPositive = 0;

        for (int i = 0; i < demention.length; i++) {
            if (demention[i] < 0) {
                if (demention[i] > maxNegative) {
                    maxNegative = demention[i];
                    numberMaxNegative = i;
                }
            }
            if (demention[i] > 0) {
                if (minPositive > demention[i]) {
                    minPositive = demention[i];
                    numberMinPositive = i;
                }
            }
        }
        if (numberMaxNegative != 0 && numberMinPositive != 0) {
            demention[numberMaxNegative] = minPositive;
            demention[numberMinPositive] = maxNegative;
        }
        System.out.println("");
        System.out.println("1. Exchange max negative and min positive elements:");
        for (int i = 0; i < demention.length; i++) {
            if (i % 14 == 0) {
                System.out.println("");
            }
            System.out.print(demention[i] + "    ");
        }
    }

    private void sumOfElementsOnEvenPositions(int demention[]) {
        int sumElements = 0;

        for (int i = 0; i < demention.length; i++) {
            if (i % 2 == 0) {
                sumElements += demention[i];
            }
        }
        System.out.println("");
        System.out.println("2. Sum of elements on even positions:");
        System.out.println(sumElements);
    }

    private void replaceNegativeValuesWith0(int demention[]) {
        for (int i = 0; i < demention.length; i++) {
            if (demention[i] < 0) {
                demention[i] = 0;
            }
        }
        System.out.println("");
        System.out.println("3. Replace negative values with 0:");
        for (int i = 0; i < demention.length; i++) {
            if (i % 14 == 0) {
                System.out.println("");
            }
            System.out.print(demention[i] + "    ");
        }
    }

    private void multiplyBy3EachPositiveElementStandingBeforeNegativeOne(int demention[]) {
        for (int i = 1; i < demention.length; i++) {
            if (demention[i] < 0) {
                if (demention[i - 1] > 0) {
                    demention[i - 1] *= 3;
                }
            }
        }
        System.out.println("");
        System.out.println("4. Multiply by 3 each positive element standing before negative one:");
        for (int i = 0; i < demention.length; i++) {
            if (i % 14 == 0) {
                System.out.println("");
            }
            System.out.print(demention[i] + "    ");
        }
    }

    private void differenceBetweenAverageAndMinElementInArray(int demention[]) {
        int sumItem = 0;
        float arithmeticMean = 0f;
        int minElement = demention[0];

        for (int i = 0; i < demention.length; i++) {
            sumItem += demention[i];
            if (demention[i] < minElement) {
                minElement = demention[i];
            }
        }
        arithmeticMean = ((float) ((float) sumItem / (int) demention.length)) - minElement;
        System.out.println("Difference between average and min element in array:");
        System.out.println(arithmeticMean);
    }

    private void elementsWhichPresentMoreThanOneTimeAndStayOnOddIndex(int demention[]) {
        String itemsMorethanOne = "";
        for (int i = 0; i < demention.length - 1; i++) {
            for (int j = i + 1; j < demention.length; j++) {
                if (demention[i] == demention[j]) {
                    if (i % 2 != 0 || j % 2 != 0) {
                        int k = itemsMorethanOne.indexOf(Integer.toString(demention[i]), 0);
                        if (k < 0) {
                            itemsMorethanOne = itemsMorethanOne.concat("   ")
                                    .concat(Integer.toString(demention[i]));
                        }
                    }
                }
            }
        }
        System.out.println("");
        System.out.println("6. Elements which present more than one time and "
                + "stay on odd index:");
        System.out.println(itemsMorethanOne.trim());
    }
}
